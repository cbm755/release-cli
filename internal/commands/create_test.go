package commands

import (
	"flag"
	"io/ioutil"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/flags"
	"gitlab.com/gitlab-org/release-cli/internal/gitlab"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
	"gitlab.com/gitlab-org/release-cli/internal/testhelpers"
)

func TestCreate(t *testing.T) {
	logger, hook := testlog.NewNullLogger()

	mhc := &gitlab.MockHTTPClient{}
	mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseSuccess](), nil).Once()

	defer mhc.AssertExpectations(t)

	create, ctx := newTestCreate(t, logger, mhc)
	require.Equal(t, "create", create.Name)
	require.Len(t, create.Flags, 9)

	err := create.Run(ctx)
	require.NoError(t, err)

	for _, entry := range hook.AllEntries() {
		assert.Contains(t, []string{"Creating Release...", "file does not exist, using string", "release created successfully!"}, entry.Message)
	}
}

func TestCreateAssets(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	tests := []struct {
		name           string
		extraArgs      []string
		expectedRunErr bool
		expectedErrMsg string
	}{
		{
			name:      "success",
			extraArgs: []string{"--assets-links-name", "asset-1", "--assets-links-url", "asset-url-1"},
		},
		{
			name: "success_multiple_links",
			extraArgs: []string{"--assets-links-name", "asset-1", "--assets-links-url", "asset-url-1",
				"--assets-links-name", "asset-2", "--assets-links-url", "asset-url-2",
				"--assets-links-name", "asset-3", "--assets-links-url", "asset-url-3"},
		},
		{
			name:           "mismatch_names_and_urls",
			extraArgs:      []string{"--assets-links-name", "asset-1"},
			expectedErrMsg: "new CreateReleaseRequest: failed to parse assets: mismatch length --assets-links-name (1) and --assets-links-url (0) should be equal",
		},
		{
			name:      "success_with_assets_link_json",
			extraArgs: []string{"--assets-link", `{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`},
		},
		{
			name: "success_with_multiple_assets_link_json",
			extraArgs: []string{"--assets-link", `{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				"--assets-link", `{"name":"Asset2","url":"https://<domain>/some/location/2","link_type":"other","filepath":"xzy2"}`,
				"--assets-link", `{"name":"Asset3","url":"https://<domain>/some/location/3","link_type":"other","filepath":"xzy3"}`},
		},
		{
			name:           "failed_with_invalid_assets_link_json",
			extraArgs:      []string{"--assets-link", "not_json"},
			expectedErrMsg: `new CreateReleaseRequest: failed to parse assets: invalid JSON: "not_json"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := gitlab.MockHTTPClient{}
			if tt.expectedErrMsg == "" {
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseSuccess](), nil).Once()
				defer mhc.AssertExpectations(t)
			}

			create, ctx := newTestCreate(t, logger, &mhc, tt.extraArgs...)
			err := create.Run(ctx)
			if tt.expectedErrMsg != "" {
				require.EqualError(t, err, tt.expectedErrMsg)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestCreateMilestonesAndReleasedAt(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	tests := []struct {
		name           string
		extraArgs      []string
		expectedRunErr bool
		expectedErrMsg string
	}{
		{
			name:      "success_milestone_and_released_at",
			extraArgs: []string{"--milestone", "v1.0", "--released-at", "2019-01-03T01:55:18.203Z"},
		},
		{
			name:      "success_multiple_milestones",
			extraArgs: []string{"--milestone", "v1.0", "--milestone", "v1.0-rc"},
		},
		{
			name:      "success_released_at",
			extraArgs: []string{"--released-at", "2019-01-03T01:55:18.203Z"},
		},
		{
			name:           "invalid_released_at",
			extraArgs:      []string{"--released-at", "2019/01/03 01:55:18"},
			expectedErrMsg: "failed to parse released-at: parsing time",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := gitlab.MockHTTPClient{}
			defer mhc.AssertExpectations(t)
			if tt.expectedErrMsg == "" {
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseSuccess](), nil).Once()
			}

			create, ctx := newTestCreate(t, logger, &mhc, tt.extraArgs...)
			err := create.Run(ctx)
			if tt.expectedErrMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrMsg)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestGetDescription(t *testing.T) {
	chdirSet := false
	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)

	defer chdir()

	logger, _ := testlog.NewNullLogger()

	file := "testdata/description.txt"
	content, err := ioutil.ReadFile(file)
	require.NoError(t, err)

	tests := []struct {
		name     string
		arg      string
		expected string
	}{
		{
			name:     "description_from_string_without_whitespace",
			arg:      "description",
			expected: "description",
		},
		{
			name:     "description_from_string_with_whitespace",
			arg:      "description from string",
			expected: "description from string",
		},
		{
			name:     "description_from_empty_string",
			arg:      "",
			expected: "",
		},
		{
			name:     "description_from_file",
			arg:      file,
			expected: string(content),
		},
		{
			name:     "description_from_file_with_path_traversal_use_as_string",
			arg:      "../testdata/description-traversal.txt",
			expected: "../testdata/description-traversal.txt",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			description, err := getDescription(tt.arg, logger)

			require.NoError(t, err)
			require.Equal(t, tt.expected, description)
		})
	}
}

func Test_newCreateReleaseReq(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	rat, err := gitlab.ParseDateTime("2019-01-03T01:55:18.203Z")
	require.NoError(t, err)

	baseCRR := gitlab.CreateReleaseRequest{
		ID:          "",
		Name:        "name",
		Description: "desc",
		TagName:     "tag",
		Ref:         "ref",
		Assets: &gitlab.Assets{Links: []*gitlab.Link{
			{Name: "asset-1", URL: "asset-url-1"},
		}},
		Milestones: []string{"m1", "m2"},
		ReleasedAt: &rat,
	}

	requiredArgs := []string{"--name", "name", "--description", "desc", "--tag-name", "tag", "--ref", "ref"}

	tests := []struct {
		name           string
		args           []string
		expected       *gitlab.CreateReleaseRequest
		expectedErrMsg string
	}{
		{
			name: "all_valid_fields",
			args: []string{"--assets-links-name", "asset-1", "--assets-links-url", "asset-url-1",
				"--milestone", "m1", "--milestone", "m2", "--released-at", "2019-01-03T01:55:18.203Z"},
			expected: &baseCRR,
		},
		{
			name: "no_assets",
			args: []string{"--milestone", "m1", "--milestone", "m2", "--released-at", "2019-01-03T01:55:18.203Z"},
			expected: newTestCreateReleaseRequest(baseCRR, func(crr *gitlab.CreateReleaseRequest) {
				crr.Assets = nil
			}),
		},
		{
			name: "no_released_at",
			args: []string{"--assets-links-name", "asset-1", "--assets-links-url", "asset-url-1",
				"--milestone", "m1", "--milestone", "m2"},
			expected: newTestCreateReleaseRequest(baseCRR, func(crr *gitlab.CreateReleaseRequest) {
				crr.ReleasedAt = nil
			}),
		},
		{
			name: "no_milestones",
			args: []string{"--assets-links-name", "asset-1", "--assets-links-url", "asset-url-1",
				"--released-at", "2019-01-03T01:55:18.203Z"},
			expected: newTestCreateReleaseRequest(baseCRR, func(crr *gitlab.CreateReleaseRequest) {
				crr.Milestones = nil
			}),
		},
		{
			name: "no_extra_args",
			args: []string{},
			expected: newTestCreateReleaseRequest(baseCRR, func(crr *gitlab.CreateReleaseRequest) {
				crr.Assets = nil
				crr.Milestones = nil
				crr.ReleasedAt = nil
			}),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			args := append(requiredArgs, tt.args...)
			set := getFlagSet(t, args...)
			ctx := cli.NewContext(nil, set, nil)

			got, err := newCreateReleaseReq(ctx, logger)
			if tt.expectedErrMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrMsg)
				return
			}
			require.NoError(t, err)

			require.Equal(t, tt.expected, got)
		})
	}
}

// to mock cli.Context we need to manually create the flagSet
func getFlagSet(t *testing.T, args ...string) *flag.FlagSet {
	t.Helper()

	set := flag.NewFlagSet(t.Name(), 0)
	aln := cli.StringSliceFlag{Name: flags.AssetsLinksName}
	_ = aln.Apply(set)
	alu := cli.StringSliceFlag{Name: flags.AssetsLinksURL}
	_ = alu.Apply(set)
	m := cli.StringSliceFlag{Name: flags.Milestone}
	_ = m.Apply(set)

	set.String(flags.Name, "", "")
	set.String(flags.Description, "", "")
	set.String(flags.TagName, "", "")
	set.String(flags.Ref, "", "")
	set.String(flags.ReleasedAt, "", "")

	err := set.Parse(args)
	require.NoError(t, err)

	return set
}

func newTestCreateReleaseRequest(base gitlab.CreateReleaseRequest, mutators ...func(*gitlab.CreateReleaseRequest)) *gitlab.CreateReleaseRequest {
	for _, mutate := range mutators {
		mutate(&base)
	}

	return &base
}

func newTestCreate(t *testing.T, logger logrus.FieldLogger, mhc gitlab.HTTPClient, extraArgs ...string) (*cli.Command, *cli.Context) {
	t.Helper()

	clientFn := func(ctx *cli.Context) gitlab.HTTPClient {
		return mhc
	}

	args := []string{"create", "--name", t.Name(), "--description", t.Name(), "--tag-name", t.Name()}
	args = append(args, extraArgs...)

	app := &cli.App{Writer: ioutil.Discard}
	set := flag.NewFlagSet(t.Name(), 0)
	set.String(flags.ServerURL, "https://gitlab.com", "")
	set.String(flags.JobToken, "token", "")

	err := set.Parse(args)
	require.NoError(t, err)

	create := Create(logger, clientFn)
	ctx := cli.NewContext(app, set, nil)

	return create, ctx
}
